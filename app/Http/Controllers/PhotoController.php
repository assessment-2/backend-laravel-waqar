<?php

namespace App\Http\Controllers;

use App\Models\Photo;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;

class PhotoController extends Controller
{
    use ApiResponser;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // fetch user's all photos
        $photos = Photo::where('user_id', auth()->user()->id)->orWhere(function($query){
            $query->where('visibility_type', 'private')->where('private_type_email', auth()->user()->email);
        })->get();
        return $this->success([
            'photos' => $photos,
        ]);
    }

    /**
     * Search the photos by
     * Date, Time, Name, extension, public, private, hidden type
     * 
     *
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        $photos = Photo::where(function($query){
            $query->where('user_id', auth()->user()->id)->orWhere(function($query){
                $query->where('visibility_type', 'private')->where('private_type_email', auth()->user()->email);
            });    
        });
        
       
        if( $request->input('name') ){
            $photos = $photos->where('image_url', 'like', '%'.$request->input('name').'%' );            
        }
        if( $request->input('extension') ){
            // dd($request->all());
            $photos = $photos->where('extension', '=' ,$request->input('extension'));
            
        }
        if( $request->input('visitbility_type') ){
            $photos = $photos->where('visitbility_type', $request->input('visitbility_type'));
        }

        if( $request->input('date_time') ){
            $photos = $photos->where('created_at', $request->input('date_time'));
        }

        $photos = $photos->get();

        return $this->success([
            'photos' => $photos
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // apply validation
        $data = $request->validate([
            // 'name' => 'required',
            'image' => 'required'
        ]);

        // save picture into folder
        if($request->file('image')){
            $file= $request->file('image');
            $filename= date('YmdHi').$file->getClientOriginalName();
            $file->move(public_path('public/gallery/'), $filename);
            $data['image_url'] = asset('/public/gallery/'.$filename);
            $info = pathinfo($filename);
            $data['extension'] = isset($info['extension']) ? $info['extension'] : null;
        }
        $photo_data = [
        // 'name' => $data['name'],
        'image_url' => $data['image_url'],
        'extension' => $data['extension'],
        'user_id' => auth()->user()->id
        ];
        // set visitbility
        if( $request->has('visibility_type')){
            $photo_data['visibility_type'] = $request->input('visibility_type');
            if( $request->input('visibility_type') === 'private' ){
                $photo_data['private_type_email'] = $request->input('private_type_email') ? $request->input('private_type_email') : null;
            }
        }

        $photo = Photo::create($photo_data);

        return $this->success([
            'photo' => $photo,
        ], 'photo uploaded');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Photo  $photo
     * @return \Illuminate\Http\Response
     */
    public function show(Photo $photo)
    {
        if( $photo->user_id != auth()->user()->id && ($photo->visibility_type === 'private' && $photo->private_type_email != auth()->user()->email)  )
            return $this->error("You don't have access to it", 401);


        return $this->success([
            'photo' => $photo,
        ]);
    }

    /**
     * create shareable link for the photo
     *
     * @param  \App\Models\Photo  $photo
     * @return \Illuminate\Http\Response
     */
    public function create_link(Photo $photo)
    {
        return $this->success([
            'link' => url('/api/photos/'.$photo->id)
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Photo  $photo
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Photo $photo)
    {
        if( $photo->user_id != auth()->user()->id && ($photo->visibility_type === 'private' && $photo->private_type_email != auth()->user()->email)  )
        return $this->error("You don't have access to it", 401);
        //define rules for validation
        $rules = [
            'visibility_type' => 'required|string'
        ];
        if( $request->input('visibility_type') === 'private' ){
            $rules['private_type_email'] = 'required|string';
        }
        // apply validation
        $data = $request->validate($rules);
        
        //set visibility and private type email incase of private type
        $photo->visibility_type = $request->input('visibility_type');
        $photo->private_type_email = $request->input('private_type_email') ? $request->input('private_type_email') : null;
        
        //update photo
        $res = $photo->save();
        if( $res )
            return $this->success(null, 'Visibility changed');

        return $this->error('Visibility could not changed');
    
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Photo  $photo
     * @return \Illuminate\Http\Response
     */
    public function destroy(Photo $photo)
    {
        if( !$photo )
            return $this->error('Photo does not exist', 404);

        if( $photo->user_id != auth()->user()->id && ($photo->visibility_type === 'private' && $photo->private_type_email != auth()->user()->email)  )
            return $this->error("You don't have access to it", 401);
        
        // delete photo
        $photo->delete();

        return $this->success(null, 'Photo Deleted');
    }
}
