<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    use ApiResponser;

    //register user
    public function register(Request $request){
        
        $data = $request->validate([
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|unique:users,email',
            'password' => 'required|string|min:8|confirmed',
            'age' => 'required'
        ]);

        // save picture into folder
        if($request->file('picture')){
            $file= $request->file('picture');
            $filename= date('YmdHi').$file->getClientOriginalName();
            $file-> move(public_path('public/pictures'), $filename);
            $data['picture']= asset('/public/pictures/'.$filename);
        }
        $user = User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            'picture' => isset($data['picture']) ? $data['picture'] : null,
            'age' => $data['age'],

        ]);

        return $this->success([
            'token' => $user->createToken('API Token')->plainTextToken
        ]);
    }

    //login user into app
    public function login(Request $request){
        //validate request data
        $data = $request->validate([
            'email' => 'required|string|email',
            'password' => 'required|string',
        ]);

        //attempt login and returns error if login details are incorrect
        if(!Auth::attempt($data)){
            return $this->error('Incorrect login details', 401);
        }

        return $this->success([
            'token' => auth()->user()->createToken('API Token')->plainTextToken
        ]);
    }

    //logout user from app
    public function logout(){
        auth()->user()->tokens()->delete();

        // return response()->json(['message' => 'Token revoked']);
        return $this->success(null, 'Token revoked');
    }


    //forget password
    public function forgotPassword(Request $request){
        
        $data = $request->validate([
            'email' => 'required|string',
            'password' => 'required|string|min:8'
        ]);

        $user = User::where('email', $data['email'])->update(['password' => Hash::make($data['password'])]);

        if(!$user)
        return $this->error('Password could not be changed', 412);

        return $this->success(null, 'Password changed');

    }
}
