<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;

class ProfileController extends Controller
{
    use ApiResponser;
    public function update(Request $request){
         
        $data = $request->validate([
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|unique:users,email,'.$request->input('id'),
            'password' => 'required|string|min:8',
            'age' => 'required'
        ]);

        if($request->file('picture')){
            $file= $request->file('picture');
            $filename= date('YmdHi').$file->getClientOriginalName();
            $file-> move(public_path('public/pictures'), $filename);
            $data['picture']= $filename;
        }

        //update user
        $res = User::find($request->input('id'))->update($data);

        if( !$res )
            return $this->error('Profile could not be Updated');
        
        $updatedUser = User::find($request->input('id'));
        return $this->success([
            'user' => $updatedUser
        ], 'Profile Updated');
    }
}
