<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\PhotoController;
use App\Http\Controllers\ProfileController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::post('/auth/register', [AuthController::class, 'register']);

Route::post('/auth/login', [AuthController::class, 'login']);
Route::post('/auth/forgot/password', [AuthController::class, 'forgotPassword']);



Route::group(['middleware' => 'auth:sanctum'],function(){
    Route::post('/profile/update', [ProfileController::class, 'update']);
    Route::get('/photos/search', [PhotoController::class, 'search']);
    Route::get('/photos/create-link/{photo}', [PhotoController::class, 'create_link']);
    Route::resource('photos', PhotoController::class)->except([
        'create','edit'
    ]);

});
/* Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
}); */
